const { PrismaClient } = require('@prisma/client')

const prisma = new PrismaClient()

const typeTransfersData = [
  {
    "title": "Initial balance",
    "nature": "INCOME"
  },
  {
    "title": "Pay order",
    "nature": "EGRESS"
  },
  {
    "title": "Transfer origin",
    "nature": "EGRESS"
  },
  {
    "title": "Transfer destination",
    "nature": "INCOME"
  },
  {
    "title": "Consignment",
    "nature": "INCOME"
  }
]

async function main() {
  console.log(`Start seeding ...`)
  for (const u of typeTransfersData) {
    const typeTransfer = await prisma.typeTransaction.create({
      data: u,
    })
    console.log(`Created Type  (${typeTransfer.title}) with id: ${typeTransfer.id}`)
  }
  console.log(`Seeding finished.`)
}

main()
  .catch((e) => {
    console.error(e)
    process.exit(1)
  })
  .finally(async () => {
    await prisma.$disconnect()
  })