<!--
title: 'AWS Simple HTTP Endpoint example in NodeJS'
description: 'This template demonstrates how to make a simple HTTP API with Node.js running on AWS Lambda and API Gateway using the Serverless Framework.'
layout: Doc
framework: v2
platform: AWS
language: nodeJS
authorLink: 'https://github.com/serverless'
authorName: 'Serverless, inc.'
authorAvatar: 'https://avatars1.githubusercontent.com/u/13742415?s=200&v=4'
-->

# Serverless Framework Node HTTP API on AWS

This template demonstrates how to make a simple HTTP API with Node.js running on AWS Lambda and API Gateway using the Serverless Framework.

This template does not include any kind of persistence (database). For more advanced examples, check out the [serverless/examples repository](https://github.com/serverless/examples/) which includes Typescript, Mongo, DynamoDB and other examples.

## Usage Offline

### Install dependencies

```
$ npm install
```

After install dependencies, make file .env:

In GitBash console

```
$ cp .env.example .env
```

### Config Database
In .env file replace names with []

PD: It is not necessary to have created the database
```
DATABASE_URL="postgresql://[USER_NAME]:[PASSWORD]@[HOST]:[PORT]/[NAME_DATABASE]"
```

### Run Migrations and Seeder

In console run this commands
```
$ npx prisma migrate dev
```

This command excute all migrations and seeders

The result should be similar to this
```
PostgreSQL database [NAME YOUR DATABASE] created at [YOUR HOST]:[YOUR PORT]

Applying migration `20211211044448_init`

The following migration(s) have been applied:

migrations/
  └─ 20211211044448_init/
    └─ migration.sql

Your database is now in sync with your schema.

✔ Generated Prisma Client (3.6.0 | library) to .\node_modules\@prisma\client in 82ms


Running seed command `node prisma/seed.js` ...
Start seeding ...
Created Type  (Initial balance) with id: 1
Created Type  (Pay order) with id: 2
Created Type  (Transfer origin) with id: 3
Created Type  (Transfer destination) with id: 4
Created Type  (Consignment) with id: 5
Seeding finished.

The seed command has been executed.

```

### Execute Serverless Offline
```
$ sls offline
or
$ serverless offline
```

### Run unit test (Jest)
```
$ npm run test
```

### Postman 
Use the file "endpoints.postman_collection.json" to mount request in postman for test endpoints manually
