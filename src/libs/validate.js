const Validator = require('jsonschema').Validator
const v = new Validator

const validateData = (data, schema) => {
  let result = v.validate(data, schema)
  if (result.errors.length > 0) {
    const errorRequest = result.errors.map(item => {
      return item.message
    });
    throw errorRequest
  } else {
    return true
  }
}

module.exports = validateData