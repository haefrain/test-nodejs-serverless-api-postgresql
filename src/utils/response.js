const response = (statusCode, body) => {
  return {
    statusCode,
    body: JSON.stringify(body)
  }
}

module.exports = {response}