const {prismaClient} = require('../config/prisma')

const ifExist = {
  async user (where) {
    const finded = await prismaClient.user.findUnique({
      where
    })
    if (!finded) {
      throw ["The user not exists"]
    }
    return finded
  }
}

module.exports = { ifExist }