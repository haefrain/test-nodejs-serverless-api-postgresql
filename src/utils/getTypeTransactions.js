const NodeCache = require('node-cache')
const myCache = new NodeCache({stdTTL: 100, checkperiod: 120})
const {prismaClient} = require('../config/prisma')

const KEY_CACHE = "LIST_TYPES_TRANSACTIONS"

const typeTransaction = {
  async getTypeTransaction () {
    let typeTransactionsCached = myCache.get(KEY_CACHE)
    if (typeTransactionsCached === undefined) {
      await prismaClient.typeTransaction.findMany().then(data => {
        myCache.set(KEY_CACHE, data)
      })
    }
    return myCache.get(KEY_CACHE)
  }
}

module.exports = { typeTransaction }