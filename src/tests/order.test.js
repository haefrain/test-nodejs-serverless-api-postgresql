const Order = require('../functions/order')
const CreateUser = require('../functions/user')

const PrepareToTest = async () => {
  let name = `UserTest_WithMoney_${Date.now()}`
  let email = name + '@test.com'
  let balance = 10000
  let response = await CreateUser.store({body: JSON.stringify({
      name,
      email,
      balance
    })})

  let userWithMoney = JSON.parse(response.body)
  return {
    userWithMoney
  }
}

const makeRequest = async (userId = null, detailOrder = null) => {
  let payload = {}

  if (userId !== null) {
    payload = {...payload, userId}
  }

  if (detailOrder !== null) {
    payload = {...payload, detailOrder}
  }

  let response = await Order.store({body: JSON.stringify(payload)})
  return JSON.parse(response.body)
}

const expectedResponseError = () => {
  return {
    "withoutUserId": ["requires property \"userId\""],
    "stringUserId": ["is not of a type(s) number"],
    "withoutNameProduct": ["requires property \"nameProduct\""],
    "withoutAmount": ["requires property \"amount\""],
    "withoutQuantity": ["requires property \"quantity\""],
    "stringAmount": ["is not of a type(s) number"],
    "stringQuantity": ["is not of a type(s) number"],
    "emptyDetailOrder": ["does not meet minimum length of 1"],
    "withoutDetailOrder": ["requires property \"detailOrder\""]
  }
}

describe("New order", () => {

  test("Order success", async () => {
    let {userWithMoney} = await PrepareToTest()
    let detailOrder = [
      {
        "nameProduct": "test1",
        "amount": 100,
        "quantity": 5
      },
      {
        "nameProduct": "test2",
        "amount": 200,
        "quantity": 2
      }
    ]
    let output = await makeRequest(userWithMoney.id, detailOrder)
    expect(Object.keys(output)[0]).toBe("id")
  })

  test("Order without money", async () => {
    let {userWithMoney} = await PrepareToTest()
    let detailOrder = [
      {
        "nameProduct": "test1",
        "amount": 15000,
        "quantity": 5
      },
      {
        "nameProduct": "test2",
        "amount": 35000,
        "quantity": 50
      }
    ]
    let output = await makeRequest(userWithMoney.id, detailOrder)
    expect(output).toBe("The user doesn't have funds")
  })

  test("Order validate data", async () => {
    let {userWithMoney} = await PrepareToTest()
    let detailOrder = [{
      "nameProduct": "test1",
      "amount": 15,
      "quantity": 5
    }]
    let output = await makeRequest(null, detailOrder)
    expect(output).toEqual(expectedResponseError().withoutUserId)

    detailOrder = [{
      "nameProduct": "test1",
      "amount": 15,
      "quantity": 5
    }]
    output = await makeRequest(userWithMoney.id + "", detailOrder)
    expect(output).toEqual(expectedResponseError().stringUserId)

    detailOrder = [{
      "amount": 15,
      "quantity": 5
    }]
    output = await makeRequest(userWithMoney.id, detailOrder)
    expect(output).toEqual(expectedResponseError().withoutNameProduct)

    detailOrder = [{
      "nameProduct": "test1",
      "quantity": 5
    }]
    output = await makeRequest(userWithMoney.id, detailOrder)
    expect(output).toEqual(expectedResponseError().withoutAmount)

    detailOrder = [{
      "nameProduct": "test1",
      "amount": 15
    }]
    output = await makeRequest(userWithMoney.id, detailOrder)
    expect(output).toEqual(expectedResponseError().withoutQuantity)

    detailOrder = [{
      "nameProduct": "test1",
      "amount": "15",
      "quantity": 5
    }]
    output = await makeRequest(userWithMoney.id, detailOrder)
    expect(output).toEqual(expectedResponseError().stringAmount)

    detailOrder = [{
      "nameProduct": "test1",
      "amount": 15,
      "quantity": "5"
    }]
    output = await makeRequest(userWithMoney.id, detailOrder)
    expect(output).toEqual(expectedResponseError().stringQuantity)

    detailOrder = []
    output = await makeRequest(userWithMoney.id, detailOrder)
    expect(output).toEqual(expectedResponseError().emptyDetailOrder)

    output = await makeRequest(userWithMoney.id, null)
    expect(output).toEqual(expectedResponseError().withoutDetailOrder)

  })
})
