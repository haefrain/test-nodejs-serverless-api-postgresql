const Consignement = require('../functions/consignement')
const CreateUser = require('../functions/user')

const PrepareToTest = async () => {
  let name = `UserTest_WithMoney_${Date.now()}`
  let email = name + '@test.com'
  let balance = 10000
  let response = await CreateUser.store({
    body: JSON.stringify({
      name,
      email,
      balance
    })
  })

  let userWithMoney = JSON.parse(response.body)
  return userWithMoney
}

const makeRequest = async (userId, amount) => {
  let input

  if (userId !== null) {
    input = {...input, userId}
  }

  if (amount !== null) {
    input = {...input, amount}
  }

  const response = await Consignement.store({body: JSON.stringify(input)})
  let output = JSON.parse(response.body)
  return {input, output}
}

const expectedResponseError = () => {
  return {
    "withoutUserId": ["requires property \"userId\""],
    "withoutAmount": ["requires property \"amount\""],
    "userIdNotExist": ["The user not exists"],
    "negativeBalance": ["must be greater than or equal to 1"],
    "stringUserId": ["is not of a type(s) number"],
    "stringAmount": ["is not of a type(s) number"]
  }
}

describe("Creation of Consignement", () => {
  test("Create consignement success", async () => {
    let user = await PrepareToTest()
    let result = await makeRequest(user.id, 100)
    expect(result.output).toEqual(result.input)
  })

  test("Validation fields", async () => {
    let user = await PrepareToTest()
    let result

    result = await makeRequest(null, 100)
    expect(result.output).toEqual(expectedResponseError().withoutUserId)

    result = await makeRequest(user.id, null)
    expect(result.output).toEqual(expectedResponseError().withoutAmount)

    result = await makeRequest(99999985, 100)
    expect(result.output).toEqual(expectedResponseError().userIdNotExist)

    result = await makeRequest(user.id, -100)
    expect(result.output).toEqual(expectedResponseError().negativeBalance)

    result = await makeRequest("werwer", 100)
    expect(result.output).toEqual(expectedResponseError().stringUserId)

    result = await makeRequest(user.id, "qweqwdwdq")
    expect(result.output).toEqual(expectedResponseError().stringAmount)

  })
})


