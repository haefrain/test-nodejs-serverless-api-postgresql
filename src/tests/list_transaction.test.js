const Transaction = require('../functions/transaction')
const CreateUser = require('../functions/user')
const Transfer = require('../functions/transfer')

const PrepareToTest = async () => {
  let name = `UserTest_WithMoney_${Date.now()}`
  let email = name + '@test.com'
  let balance = 10000
  let response = await CreateUser.store({body: JSON.stringify({
      name,
      email,
      balance
    })})

  let userWithMoney = JSON.parse(response.body)

  name = `UserTest_WithoutMoney_${Date.now()}`
  email = name + '@test.com'
  balance = 0
  response = await CreateUser.store({body: JSON.stringify({
      name,
      email,
      balance
    })})

  let userWithoutMoney = JSON.parse(response.body)

  let payloadTransfer = {
    userMadeId: userWithMoney.id,
    userAffectedId: userWithoutMoney.id,
    amount: 500
  }

  let responseTransfer = await Transfer.store({body: JSON.stringify(payloadTransfer)})
  let transfer = JSON.parse(responseTransfer.body)

  return { userWithMoney, userWithoutMoney, transfer }
}

const makeRequest = async (id = null, typeTransactionId = null, userMadeId = null, userAffectedId = null) => {
  let payload = {}

  if (id !== null) {
    payload = {...payload, id}
  }

  if (typeTransactionId !== null) {
    payload = {...payload, typeTransactionId}
  }

  if (userMadeId !== null) {
    payload = {...payload, userMadeId}
  }

  if (userAffectedId !== null) {
    payload = {...payload, userAffectedId}
  }

  let response = await Transaction.index({body: JSON.stringify(payload)})
  return JSON.parse(response.body)
}

const expectedResponseError = () => {
  return {
    "isString": ["is not of a type(s) number",]
  }
}

describe("Make transfers between users", () => {

  test("List transaction success", async () => {
    const { userWithMoney, userWithoutMoney, transfer } = await PrepareToTest()
    let output = await makeRequest()
    expect(Array.isArray(output)).toEqual(true)

    output = await makeRequest(1)
    expect(Array.isArray(output)).toEqual(true)

    output = await makeRequest(null, 1)
    expect(Array.isArray(output)).toEqual(true)

    output = await makeRequest(null, null, userWithMoney.id)
    expect(Array.isArray(output)).toEqual(true)

    output = await makeRequest(null, null, null, userWithoutMoney.id)
    expect(Array.isArray(output)).toEqual(true)
  })

  test("Validate data", async () => {
    const { userWithMoney, userWithoutMoney, transfer } = await PrepareToTest()

    output = await makeRequest("1")
    expect(output).toEqual(expectedResponseError().isString)

    output = await makeRequest(null, "1")
    expect(output).toEqual(expectedResponseError().isString)

    output = await makeRequest(null, null, userWithMoney.id + "")
    expect(output).toEqual(expectedResponseError().isString)

    output = await makeRequest(null, null, null, userWithoutMoney.id + "")
    expect(output).toEqual(expectedResponseError().isString)
  })

})
