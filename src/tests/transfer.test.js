const Transfer = require('../functions/transfer')
const CreateUser = require('../functions/user')

const PrepareToTest = async () => {
  let name = `UserTest_WithMoney_${Date.now()}`
  let email = name + '@test.com'
  let balance = 10000
  let response = await CreateUser.store({body: JSON.stringify({
      name,
      email,
      balance
    })})

  let userWithMoney = JSON.parse(response.body)

  name = `UserTest_WithoutMoney_${Date.now()}`
  email = name + '@test.com'
  balance = 0
  response = await CreateUser.store({body: JSON.stringify({
      name,
      email,
      balance
    })})

  let userWithoutMoney = JSON.parse(response.body)
  return {
    userWithMoney,
    userWithoutMoney
  }
}

const makeRequest = async (userMadeId = null, userAffectedId = null, amount = null) => {
  let payload

  if (userMadeId !== null) {
    payload = {...payload, userMadeId}
  }

  if (userAffectedId !== null) {
    payload = {...payload, userAffectedId}
  }

  if (amount !== null) {
    payload = {...payload, amount}
  }

  let response = await Transfer.store({body: JSON.stringify(payload)})
  return JSON.parse(response.body)
}

const expectedResponseError = () => {
  return {
    "withoutMadeId": ["requires property \"userMadeId\""],
    "withoutAffectedId": ["requires property \"userAffectedId\""],
    "withoutAmount": ["requires property \"amount\""],
    "stringInMadeId": ["is not of a type(s) number",],
    "stringInAffectedId": ["is not of a type(s) number",],
    "stringAmount": ["is not of a type(s) number",]
  }
}

describe("Make transfers between users", () => {

  test("Transfer success", async () => {
    let users = await PrepareToTest()
    let output = await makeRequest(users.userWithMoney.id, users.userWithoutMoney.id, 500)
    expect(output).toBe("Transfer success")
  })

  test("Transfer validate data", async () => {
    let output = await makeRequest(null, 1, 500)
    expect(output).toEqual(expectedResponseError().withoutMadeId)

    output = await makeRequest(1, null, 500)
    expect(output).toEqual(expectedResponseError().withoutAffectedId)

    output = await makeRequest(1, 1, null)
    expect(output).toEqual(expectedResponseError().withoutAmount)

    output = await makeRequest("123", 1, 500)
    expect(output).toEqual(expectedResponseError().stringInMadeId)

    output = await makeRequest(1, "123", 500)
    expect(output).toEqual(expectedResponseError().stringInAffectedId)

    output = await makeRequest(1, 1, "500")
    expect(output).toEqual(expectedResponseError().stringAmount)
  })
})
