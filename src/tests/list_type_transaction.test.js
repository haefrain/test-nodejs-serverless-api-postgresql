const TypeTransaction = require('../functions/typeTransaction')

describe("List types transactions", () => {
  test("List", async () => {
    let response = await TypeTransaction.index()
    let output = JSON.parse(response.body)
    expect(Array.isArray(output)).toBe(true)
    expect(output.length > 0).toBe(true)
  })
})
