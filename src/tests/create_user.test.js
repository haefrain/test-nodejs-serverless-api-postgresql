const User = require('../functions/user')

const makeRequest = async (name = null, email = false, repeatEmail = false, balance = 500, emailWithOtherFormat = null) => {
  let input = {}
  if (name !== null) {
    input = {...input, name}
  }

  if (email && !repeatEmail && emailWithOtherFormat === null) {
    input = {...input, email: `${name}_${Date.now()}@test.com`}
  } else if (email && repeatEmail && emailWithOtherFormat === null) {
    input = {...input, email: `${name}@test.com`}
  } else if (emailWithOtherFormat !== null) {
    input = {...input, email: emailWithOtherFormat}
  }

  if (balance !== null) {
    input = {...input, balance}
  }
  const response = await User.store({body: JSON.stringify(input)})
  let output = JSON.parse(response.body)
  return {input, output}
}

const expectedResponseError = () => {
  return {
    "withOutName": ["requires property \"name\""],
    "withOutEmail": ["requires property \"email\""],
    "repeatEmail": ["The email has been exists"],
    "negativeBalance": ["must be greater than or equal to 0"],
    "numberInName": ["is not of a type(s) string"],
    "numberInEmail": ["is not of a type(s) string"],
    "emailWithoutFormat": ["does not conform to the \"email\" format"],
  }
}

describe("Creation of user", () => {
  const name = `test_${Date.now()}`
  test("Create user", async () => {
    const result = await makeRequest(name, true, false, 500)
    expect(Object.keys(result.output).length > 0).toEqual(true)
  })

  test("Validation fields", async () => {
    let result
    result = await makeRequest(null, true)
    expect(result.output).toEqual(expectedResponseError().withOutName)

    result = await makeRequest(name, false)
    expect(result.output).toEqual(expectedResponseError().withOutEmail)

    result = await makeRequest(name, true, true)
    expect(Object.keys(result.output).length > 0).toEqual(true)

    result = await makeRequest(name, true, true)
    expect(result.output).toEqual(expectedResponseError().repeatEmail)

    result = await makeRequest(name, true, false, 0)
    expect(Object.keys(result.output).length > 0).toEqual(true)

    result = await makeRequest(name, true, false, null)
    expect(Object.keys(result.output).length > 0).toEqual(true)

    result = await makeRequest(name, true, false, -500)
    expect(result.output).toEqual(expectedResponseError().negativeBalance)

    result = await makeRequest(123, true, false, 500)
    expect(result.output).toEqual(expectedResponseError().numberInName)

    result = await makeRequest(name, true, false, 500, 123)
    expect(result.output).toEqual(expectedResponseError().numberInEmail)

    result = await makeRequest(name, true, false, 500, name)
    expect(result.output).toEqual(expectedResponseError().emailWithoutFormat)
  })
})


