const {UserService} = require('../Services')
const ValidateData = require('../libs/validate')
const UserSchema = require('../JsonSchema/User.schema.json')
const {response} = require('../utils/response')

module.exports.store = async (event) => {
  try {
    const data = JSON.parse(event.body)

    ValidateData(data, UserSchema)

    let result = await UserService.createUser(data)
    if (result.success) {
      return await response(200, result.data)
    } else {
      return await response(422, result.data)
    }
  } catch (e) {
    return await response(422, e)
  }
}
