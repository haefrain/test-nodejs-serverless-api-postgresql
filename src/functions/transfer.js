const {TransferService} = require('../Services')
const ValidateData = require('../libs/validate')
const TransferSchema = require('../JsonSchema/Transfer.schema.json')
const {response} = require('../utils/response')

module.exports.store = async (event) => {
  try {
    const data = JSON.parse(event.body)

    ValidateData(data, TransferSchema)

    let result = await TransferService.createTransfer(data)
    if (result.success) {
      return response(200, result.data)
    } else {
      return response(422, result.data)
    }
  } catch (e) {
    return response(422, e)
  }
}
