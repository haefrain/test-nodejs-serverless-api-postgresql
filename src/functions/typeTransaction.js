const { typeTransaction } = require('../utils/getTypeTransactions')
const {response} = require('../utils/response')

module.exports.index = async (event) => {
  try {
    const result = await typeTransaction.getTypeTransaction()
    return response(200, result)
  } catch (e) {
    return response(422, e)
  }
}
