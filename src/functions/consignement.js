const {ConsignementService} = require('../Services')
const ValidateData = require('../libs/validate')
const ConsignementSchema = require('../JsonSchema/Consignement.schema.json')
const {response} = require('../utils/response')

module.exports.store = async (event) => {
  try {
    const data = JSON.parse(event.body)

    ValidateData(data, ConsignementSchema)

    let result = await ConsignementService.createConsignement(data)
    if (result.success) {
      return response(200, result.data)
    } else {
      return response(422, result.data)
    }
  } catch (e) {
    return response(422, e)
  }
}
