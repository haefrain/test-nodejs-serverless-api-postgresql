const {TransactionService} = require('../Services')
const ValidateData = require('../libs/validate')
const ListTransactionSchema = require('../JsonSchema/ListTransaction.schema.json')
const {response} = require('../utils/response')

module.exports.index = async (event) => {
  try {
    const data = JSON.parse(event.body)

    ValidateData(data, ListTransactionSchema)

    let result = await TransactionService.transaction.listTransaction(data)

    if (result.success) {
      return response(200, result.data)
    } else {
      return response(422, result.data)
    }
  } catch (e) {
    return response(422, e)
  }
}
