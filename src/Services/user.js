const {prismaClient} = require('../config/prisma')
const {transaction} = require('./transaction')

const TYPE_TRANSACTION = 1 //Initial Balance

const UserService = {
    async createUser (user) {
        let newTransaction = true
        try {
          const searchEmail = await prismaClient.user.findUnique({
            where: {
              email: user.email
            }
          })

          if (searchEmail) {
              throw ["The email has been exists"];
          }

          const newUser = await prismaClient.user.create({
              data: {...user, balance: 0},
          });


          if (user.balance > 0) {

              newTransaction = await transaction.makeTransaction({
                  typeTransactionId: TYPE_TRANSACTION,
                  userMadeId: newUser.id,
                  userAffectedId: newUser.id,
                  amount: user.balance
              })
              if (!newTransaction.success) {
                  throw newTransaction.message
              }
          }

          return {
              success: true,
              data: newUser
          }
        } catch(e) {
            return {
                success: false,
                data: e
            }
        }
    }
}

module.exports = UserService