const {prismaClient, Prisma} = require('../config/prisma')
const {transaction} = require('./transaction')
const { ifExist } = require('../utils/ifExist')

const TYPE_TRANSACTION = 2 //Pay Order

const OrderService = {
  async createOrder (data) {
    try {

      const user = await ifExist.user({id: data.userId})
      let totalOrder = 0

      data.detailOrder.forEach(element => totalOrder += parseInt(element.amount) * parseInt(element.quantity))

      let newBalance = user.balance - totalOrder

      if (newBalance < 0) {
        throw "The user doesn't have funds"
      }

      let orderWithDetail = Prisma.UserCreateInput

      orderWithDetail = {
        userId: data.userId,
        DetailOrder: {
          create: data.detailOrder
        }
      }

      newOrder = await prismaClient.order.create({data: orderWithDetail})

      newTransaction = await transaction.makeTransaction({
        typeTransactionId: TYPE_TRANSACTION,
        userMadeId: data.userId,
        userAffectedId: data.userId,
        amount: totalOrder
      })
      if (!newTransaction.success) {
        throw newTransaction.message
      }

      return {
        success: true,
        data: newOrder
      }
    } catch(e) {
      return {
        success: false,
        data: e
      }
    }
  }
}

module.exports = OrderService;