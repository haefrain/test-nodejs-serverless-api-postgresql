const {prismaClient} = require('../config/prisma')
const {transaction} = require('./transaction')
const { ifExist } = require('../utils/ifExist')

const TYPE_TRANSACTION_OUT = 3 //Transfer Origin
const TYPE_TRANSACTION_IN = 4 //Transfer Destination

const TransferService = {
  async createTransfer (data) {
    try {
      const userMade = await ifExist.user({id: data.userMadeId})
      await ifExist.user({id: data.userAffectedId})

      if ((parseInt(userMade.balance) - parseInt(data.amount)) < 0) {
        throw "The user doesn't have funds"
      }

      const newTransactionIn = await transaction.makeTransaction({
        typeTransactionId: TYPE_TRANSACTION_OUT,
        userMadeId: data.userMadeId,
        userAffectedId: data.userMadeId,
        amount: data.amount
      })

      if (!newTransactionIn.success) {
        throw newTransactionIn.message
      }

      const newTransactionOut = await transaction.makeTransaction({
        typeTransactionId: TYPE_TRANSACTION_IN,
        userMadeId: data.userMadeId,
        userAffectedId: data.userAffectedId,
        amount: data.amount
      })

      if (!newTransactionOut.success) {
        throw newTransactionOut.message
      }

      return {
        success: true,
        data: "Transfer success"
      }
    } catch(e) {
      return {
        success: false,
        data: e
      }
    }
  }
}

module.exports = TransferService