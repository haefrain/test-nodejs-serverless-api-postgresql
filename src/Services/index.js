const UserService = require('./user')
const ConsignementService = require('./consignement')
const TransferService = require('./transfer')
const OrderService = require('./order')
const TransactionService = require('./transaction')
module.exports = {UserService, ConsignementService, TransferService, OrderService, TransactionService}
