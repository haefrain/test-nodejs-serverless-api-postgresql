const {prismaClient} = require('../config/prisma')
const { typeTransaction } = require('../utils/getTypeTransactions')

const transaction = {
  async makeTransaction(data) {
    try {
      let listTypeTransactions = await typeTransaction.getTypeTransaction()
      let typeTransactionObject = listTypeTransactions?.find(item => {
        return data.typeTransactionId === item.id
      })

      if (!typeTransactionObject) {
        throw "Not exists type of transaction"
      }
      await prismaClient.$transaction(async (prismaClient) => {

        const userAffected = await prismaClient.user.findUnique({
          where: {
            id: data.userAffectedId
          }
        })

        if (!userAffected) {
          throw "Not exist user affected"
        }

        let newBalance = userAffected.balance
        if (typeTransactionObject.nature === 'INCOME') {
          newBalance += parseInt(data.amount)
        } else if (typeTransactionObject.nature === 'EGRESS') {
          newBalance -= parseInt(data.amount)

          if (newBalance < 0) {
            throw "You don't have sufficient funds"
          }

        } else {
          throw "Not detected nature of transaction"
        }

        await prismaClient.user.update({
          where: {
            id: data.userAffectedId
          },
          data: {
            balance: newBalance
          }
        })

        await prismaClient.transaction.create({
          data
        })

        return true
      })


      return {success: true, message: ""}
    } catch (e) {
      return {success: false, message: e}
    }

  },
  async listTransaction (where) {
    try {
      let result
      if (where) {
        result = await prismaClient.transaction.findMany({
          where: where,
          include: {
            typeTransaction: true
          }
        })

      } else {
        result = await prismaClient.transaction.findMany({
          include: {
            typeTransaction: true
          }
        })
      }
      return {success: true, data: result}
    } catch (e) {
      return {success: false, data: []}
    }
  }
}

module.exports = { transaction }