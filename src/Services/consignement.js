const {transaction} = require('./transaction')
const { ifExist } = require('../utils/ifExist')

const TYPE_TRANSACTION = 5 //Consignement

const ConsignementService = {
  async createConsignement (data) {
    try {
      await ifExist.user({id: data.userId})

      newTransaction = await transaction.makeTransaction({
        typeTransactionId: TYPE_TRANSACTION,
        userMadeId: data.userId,
        userAffectedId: data.userId,
        amount: data.amount
      })

      if (!newTransaction.success) {
        throw newTransaction.message
      }

      return {
        success: true,
        data
      }
    } catch(e) {
      return {
        success: false,
        data: e
      }
    }
  }
}

module.exports = ConsignementService